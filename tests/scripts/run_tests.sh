#!/usr/bin/bash

# make sure we have rteval installed
if rpm -q --quiet rteval; then
    :
else
    sudo dnf install -y rteval
    if [[ $? != 0 ]]; then
	echo "install of rteval failed!"
	exit 1
    fi
fi

rteval --help

if [[ $? != 0 ]]; then
    exit 2
fi

sudo rteval -s

if [[ $? != 0 ]]; then 
    exit 3
fi

exit 0
